from typing import List, Any, Iterator

from .utils import excel_date


class SheetTableRow:
    def __init__(self, table: 'SheetTable', cells: List[Any]):
        self.table = table
        self.cells = cells

    def get_cell(self, name, default=None, must_exists=False):
        name = name.strip().lower()
        try:
            idx = self.table.headers.index(name)
        except ValueError:
            if not must_exists:
                raise ValueError
            return default
        if idx < len(self.cells):
            return self.cells[idx]
        else:
            if not must_exists:
                raise ValueError
            return default

    def get_cell_number(self, name, must_exists=False):
        name = name.strip().lower()
        val = self.get_cell(name, must_exists=must_exists)
        if val is None:
            return None
        elif isinstance(val, str):
            val = val.strip()
            if len(val) == 0:
                return None
            elif "#NUM!" in val:
                return None
            else:
                raise ValueError("Invalid value in cell ({0}), got: /{1}/, expected num".format(name, val))
        else:
            return val

    def get_cell_date(self, name, must_exists=False):
        name = name.strip().lower()
        val = self.get_cell(name, must_exists=must_exists)
        if val is None:
            return None
        else:
            return excel_date(val)


class SheetTable:
    def __init__(self, data):
        self.raw_data = data
        self.headers = None
        self.rows: List[SheetTableRow] = None
        self._create_rows()

    def _create_rows(self):
        self.headers = [x.lower().strip() for x in self.raw_data[0]]
        self.rows = [SheetTableRow(self, x) for x in self.raw_data[1:]]

    def get_column(self, name):
        return [x.get_cell(name) for x in self.rows]

    def __iter__(self) -> Iterator[SheetTableRow]:
        return iter(self.rows)
