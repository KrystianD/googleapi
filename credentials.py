from typing import List

import json
import oauth2client.service_account


class Credentials:
    def __init__(self, google_credentials):
        self.google_credentials = google_credentials

    @staticmethod
    def from_service_account_str(data, scopes: List[str], delegate_email: str = None):
        credentials = oauth2client.service_account.ServiceAccountCredentials.from_json_keyfile_dict(json.loads(data.strip()), scopes)
        if delegate_email is not None:
            credentials = credentials.create_delegated(delegate_email)
        return Credentials(credentials)

    @staticmethod
    def from_service_account_dict(data, scopes: List[str], delegate_email: str = None):
        credentials = oauth2client.service_account.ServiceAccountCredentials.from_json_keyfile_dict(data, scopes)
        if delegate_email is not None:
            credentials = credentials.create_delegated(delegate_email)
        return Credentials(credentials)

    @staticmethod
    def from_service_account_path(path: str, scopes: List[str], delegate_email: str = None):
        credentials = oauth2client.service_account.ServiceAccountCredentials.from_json_keyfile_name(path, scopes)
        if delegate_email is not None:
            credentials = credentials.create_delegated(delegate_email)
        return Credentials(credentials)
