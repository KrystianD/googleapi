import datetime


def letters_to_idx(letters: str):
    import xlsxwriter.utility
    return xlsxwriter.utility.xl_cell_to_rowcol("{}1".format(letters))[1] + 1


def idx_to_letters(idx: int):
    import xlsxwriter.utility
    return xlsxwriter.utility.xl_col_to_name(idx - 1)


def excel_date(date1):
    temp = datetime.datetime(1899, 12, 30)  # Note, not 31st Dec but 30th!
    if isinstance(date1, datetime.datetime):
        delta = date1 - temp
    else:
        delta = date1 - temp.date()
    return float(delta.days) + (float(delta.seconds) / 86400)
