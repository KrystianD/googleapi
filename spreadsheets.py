import datetime
from typing import Union, List, Any, Optional
import decimal

import enum
import re
from colour import Color

from .utils import letters_to_idx, idx_to_letters, excel_date
from .credentials import Credentials


class SpreadsheetAlignment(enum.Enum):
    Left = "LEFT"
    Center = "CENTER"
    Right = "RIGHT"


class SpreadsheetRangeType(enum.Enum):
    FullRange = "FullRange"
    ColumnsGroup = "ColumnsGroup"
    RowsGroup = "RowsGroup"
    Cell = "Cell"


class SpreadsheetSheetPlacement(enum.Enum):
    Begin = 0
    End = None


class SpreadsheetsRange:
    def __init__(self):
        self.sheet_name = None
        self.row_start = None
        self.row_end = None
        self.col_start = None
        self.col_end = None

    @property
    def type(self):
        has_col = self.col_start is not None and self.col_end is not None
        has_row = self.row_start is not None and self.row_end is not None

        if has_col and has_row:
            return SpreadsheetRangeType.Cell
        elif has_col and not has_row:
            return SpreadsheetRangeType.ColumnsGroup
        elif has_row and not has_col:
            return SpreadsheetRangeType.RowsGroup
        else:
            return SpreadsheetRangeType.FullRange

    def to_str(self):
        is_full_range = self.col_start is None and \
                        self.row_start is None and \
                        self.col_end is None and \
                        self.row_end is None

        if is_full_range:
            rng = "1:999999"
        else:
            rng = "{}{}:{}{}".format(
                    idx_to_letters(self.col_start) if self.col_start is not None else "",
                    self.row_start or "",
                    idx_to_letters(self.col_end) if self.col_end is not None else "",
                    self.row_end or "",
            )

        if self.sheet_name is not None:
            rng = "{}!{}".format(self.sheet_name, rng)

        return rng

    def to_api(self, sheet: 'Sheet' = None):
        _range = {}
        if self.row_start is not None: _range["startRowIndex"] = self.row_start - 1
        if self.row_end is not None: _range["endRowIndex"] = self.row_end - 1 + 1
        if self.col_start is not None: _range["startColumnIndex"] = self.col_start - 1
        if self.col_end is not None: _range["endColumnIndex"] = self.col_end - 1 + 1

        if sheet is not None:
            _range["sheetId"] = sheet.sheet_id
            if self.sheet_name is not None and sheet.title != self.sheet_name:
                raise ValueError("Range sheet name doesn't match sheet id")

        return _range

    @staticmethod
    def parse(data: Union[str, 'SpreadsheetsRange']):
        if isinstance(data, str):
            return SpreadsheetsRange.create_from_str(data)
        elif isinstance(data, SpreadsheetsRange):
            return data
        else:
            raise ValueError("Invalid data")

    @staticmethod
    def create_from_str(rng: str):
        r = SpreadsheetsRange()

        if rng is None or len(rng) == 0:
            return r

        m = re.match("^([^!]+!)?([A-Z]+)?(\d+)?(?::([A-Z]+)?(\d+)?)?$", rng)

        r.sheet_name = m.group(1)
        if r.sheet_name is not None:
            r.sheet_name = r.sheet_name[:-1]
        col_start = m.group(2)
        row_start = m.group(3)
        col_end = m.group(4)
        row_end = m.group(5)

        has_col = col_start is not None and col_end is not None
        has_row = row_start is not None and row_end is not None

        r.col_start = letters_to_idx(col_start) if col_start is not None else None
        r.row_start = int(row_start) if row_start is not None else None
        r.col_end = letters_to_idx(col_end) if col_end is not None else None
        r.row_end = int(row_end) if row_end is not None else None

        return r

    @staticmethod
    def create_from_rect(col_start: int, row_start: int, col_end: Optional[int], row_end: Optional[int],
                         sheet_name=None):
        r = SpreadsheetsRange()
        r.row_start = row_start
        r.row_end = row_end
        r.col_start = col_start
        r.col_end = col_end
        r.sheet_name = sheet_name
        return r

    @staticmethod
    def create_from_columns(cnt: int, column_start: int = 1, sheet_name=None):
        r = SpreadsheetsRange()
        r.col_start = column_start
        r.col_end = column_start + cnt - 1
        r.sheet_name = sheet_name
        return r


class SpreadsheetsCoordinate:
    def __init__(self):
        self.sheet_name = None
        self.row = None
        self.col = None

    def to_str(self):
        has_col = self.col is not None
        has_row = self.row is not None

        if has_col and has_row:
            rng = "{}{}".format(idx_to_letters(self.col), self.row)

        elif has_col and not has_row:
            rng = "{}".format(idx_to_letters(self.col))

        elif has_row and not has_col:
            rng = "{}".format(self.row)

        else:
            assert False

        if self.sheet_name is not None:
            rng = "{}!{}".format(self.sheet_name, rng)

        return rng

    def to_api(self, sheet: 'Sheet' = None):
        data = {}
        if self.row is not None: data["rowIndex"] = self.row - 1
        if self.col is not None: data["columnIndex"] = self.col - 1

        if sheet is not None:
            data["sheetId"] = sheet.sheet_id
            if self.sheet_name is not None and sheet.title != self.sheet_name:
                raise ValueError("Range sheet name doesn't match sheet id")

        return data

    @staticmethod
    def parse(data: Union[str, 'SpreadsheetsCoordinate']):
        if isinstance(data, str):
            return SpreadsheetsCoordinate.create_from_str(data)
        elif isinstance(data, SpreadsheetsCoordinate):
            return data
        else:
            raise ValueError("Invalid data")

    @staticmethod
    def create_from_str(rng: str):
        r = SpreadsheetsCoordinate()

        m = re.match("^([^!]+!)?([A-Z]+)(\d+)?", rng)

        r.sheet_name = m.group(1)
        if r.sheet_name is not None:
            r.sheet_name = r.sheet_name.rstrip("!")
        col = m.group(2)
        row = m.group(3)

        has_col = col is not None
        has_row = row is not None

        if has_col and has_row:
            r.col = letters_to_idx(col)
            r.row = int(row)

        elif has_col and not has_row:
            r.col = letters_to_idx(col)

        elif has_row and not has_col:
            r.row = int(row)

        else:
            assert False

        return r


class SpreadsheetsAPI:
    def __init__(self, credentials: Credentials):
        import httplib2
        from googleapiclient import discovery

        http = credentials.google_credentials.authorize(httplib2.Http())
        discoveryUrl = 'https://sheets.googleapis.com/$discovery/rest?version=v4'
        service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)

        self.service = service

    def get_spreadsheet(self, spreadsheet_id: str):
        return Spreadsheet(self, spreadsheet_id)

    @staticmethod
    def columns_count_to_range(cnt: int, column_start: int = 1, sheet_name=None):
        assert cnt > 0
        assert column_start > 0

        import xlsxwriter.utility
        rng1 = xlsxwriter.utility.xl_col_to_name(column_start - 1)
        rng2 = xlsxwriter.utility.xl_col_to_name(cnt + column_start - 1 - 1)

        rng = "{}:{}".format(rng1, rng2)
        if sheet_name is not None:
            rng = "{}!{}".format(sheet_name, rng)

        return rng


class Sheet:
    def __init__(self, spreadsheet: 'Spreadsheet', data=None):
        self.spreadsheet = spreadsheet
        self.title = None
        self.sheet_id = None
        if data is not None:
            self.title = data["properties"]["title"]
            self.sheet_id = data["properties"]["sheetId"]

    @staticmethod
    def create_from_id(spreadsheet: 'Spreadsheet', sheet_id: int):
        s = Sheet(spreadsheet)
        s.sheet_id = sheet_id
        return s


class Spreadsheet:
    def __init__(self, api: SpreadsheetsAPI, spreadsheet_id: str):
        self.api = api
        self.spreadsheet_id = spreadsheet_id

        self.has_meta = False
        self.title = None
        self.sheets: List[Sheet] = None

    def create_builder(self):
        return SpreadsheetsBuilder(self)

    def fetch_meta(self):
        request = self.api.service.spreadsheets().get(spreadsheetId=self.spreadsheet_id).execute()

        self.title = request["properties"]["title"]
        self.sheets = [Sheet(self, x) for x in request["sheets"]]
        self.has_meta = True

    def get_sheet_by_name(self, title: str) -> Sheet:
        if not self.has_meta:
            raise Exception("No metadata")
        for s in self.sheets:
            if s.title == title:
                return s

    def update_values(self, range: Union[str, SpreadsheetsRange], values, parse_values=False):
        if not isinstance(range, SpreadsheetsRange):
            range = SpreadsheetsRange.create_from_str(range)

        range_name = range.to_str()

        body_values = {"values": values}
        body_range = {"range": range_name}
        self.api.service.spreadsheets().values().clear(
                spreadsheetId=self.spreadsheet_id,
                range=range_name,
                body=body_range).execute()

        self.api.service.spreadsheets().values().update(
                spreadsheetId=self.spreadsheet_id,
                range=range_name, body=body_values,
                valueInputOption="USER_ENTERED" if parse_values else "RAW").execute()

    def fetch_values_simple(self, range: Union[str, SpreadsheetsRange] = None) -> List[List[Any]]:
        if not isinstance(range, SpreadsheetsRange):
            range = SpreadsheetsRange.create_from_str(range)

        range_name = range.to_str()

        result = self.api.service.spreadsheets().values().get(spreadsheetId=self.spreadsheet_id,
                                                              range=range_name,
                                                              valueRenderOption="UNFORMATTED_VALUE",
                                                              dateTimeRenderOption="SERIAL_NUMBER").execute()
        return result["values"]


class SpreadsheetsBuilder:
    def __init__(self, spreadsheet: Spreadsheet):
        self.spreadsheet = spreadsheet
        self.requests = []

    def execute(self):
        body = {
            "requests": self.requests
        }
        self.spreadsheet.api.service.spreadsheets().batchUpdate(spreadsheetId=self.spreadsheet.spreadsheet_id,
                                                                body=body).execute()

    def clear_cells(self,
                    sheet: Union[int, Sheet],
                    start: Union[str, SpreadsheetsCoordinate] = None,
                    range: Union[str, SpreadsheetsRange] = None) -> 'SpreadsheetsBuilder':
        self.update_cells(sheet=sheet, start=start, range=range, values=[])
        return self

    def update_cells(self,
                     sheet: Sheet,
                     values: List[List[Any]],
                     start: Union[str, SpreadsheetsCoordinate] = None,
                     range: Union[str, SpreadsheetsRange] = None) -> 'SpreadsheetsBuilder':
        if start is None and range is None:
            start = SpreadsheetsCoordinate()
            start.col = 1
            start.row = 1

        def create_cell(v):
            if isinstance(v, str):
                v = {"stringValue": v}
            elif isinstance(v, int) or isinstance(v, float):
                v = {"numberValue": v}
            elif isinstance(v, bool):
                v = {"boolValue": v}
            elif isinstance(v, decimal.Decimal):
                v = {"numberValue": float(v)}
            elif isinstance(v, datetime.date):
                v = {"numberValue": excel_date(v)}
            elif isinstance(v, datetime.datetime):
                v = {"numberValue": excel_date(v)}
            elif v is None:
                v = None
            else:
                raise ValueError("Unknown cell type: {}".format(type(v)))

            data = {
                "userEnteredValue": v,
            }
            return data

        d = {
            "updateCells": {
                "rows": [
                    {"values": [create_cell(cell) for cell in row]} for row in values
                ],
                "fields": "userEnteredValue"
            }
        }

        if range is not None:
            d["updateCells"]["range"] = SpreadsheetsRange.parse(range).to_api(sheet=sheet)

        elif start is not None:
            d["updateCells"]["start"] = SpreadsheetsCoordinate.parse(start).to_api(sheet=sheet)

        self.requests.append(d)
        return self

    def format_cells(self,
                     sheet: Sheet,
                     range: Union[str, SpreadsheetsRange],
                     bg_color: Color = None, fg_color: Color = None,
                     font_size: int = None, bold: bool = None,
                     horizontal_alignment: SpreadsheetAlignment = None) -> 'SpreadsheetsBuilder':
        if range is not None:
            range = SpreadsheetsRange.parse(range)

        userEnteredFormat = {
            "textFormat": {}
        }

        if bg_color is not None:
            userEnteredFormat["backgroundColor"] = {
                "red": bg_color.get_red(),
                "green": bg_color.get_green(),
                "blue": bg_color.get_blue(),
            }

        if fg_color is not None:
            userEnteredFormat["textFormat"]["foregroundColor"] = {
                "red": fg_color.get_red(),
                "green": fg_color.get_green(),
                "blue": fg_color.get_blue(),
            }

        if font_size is not None:
            userEnteredFormat["textFormat"]["fontSize"] = font_size

        if bold is not None:
            userEnteredFormat["textFormat"]["bold"] = bold

        if horizontal_alignment is not None:
            userEnteredFormat["horizontalAlignment"] = horizontal_alignment.value

        self.requests.append({
            "repeatCell": {
                "range": range.to_api(sheet),
                "cell": {
                    "userEnteredFormat": userEnteredFormat,
                },
                "fields": "userEnteredFormat(backgroundColor,textFormat,horizontalAlignment)"
            }
        })
        return self

    def freeze_rows(self, sheet: Sheet, frozen_rows_count: int) -> 'SpreadsheetsBuilder':
        self.requests.append({
            "updateSheetProperties": {
                "properties": {
                    "sheetId": sheet.sheet_id,
                    "gridProperties": {
                        "frozenRowCount": frozen_rows_count
                    }
                },
                "fields": "gridProperties.frozenRowCount"
            }
        })
        return self

    def freeze_columns(self, sheet: Sheet, frozen_cols_count: int) -> 'SpreadsheetsBuilder':
        self.requests.append({
            "updateSheetProperties": {
                "properties": {
                    "sheetId": sheet.sheet_id,
                    "gridProperties": {
                        "frozenColumnCount": frozen_cols_count
                    }
                },
                "fields": "gridProperties.frozenColumnCount"
            }
        })
        return self

    def set_columns_widths(self, sheet: Sheet, *args: List[int], column_start: int = 1) -> 'SpreadsheetsBuilder':
        for idx, width in enumerate(args):
            self.requests.append({
                "updateDimensionProperties": {
                    "range": {
                        "sheetId": sheet.sheet_id,
                        "dimension": "COLUMNS",
                        "startIndex": column_start - 1 + idx,
                        "endIndex": column_start - 1 + idx + 1,
                    },
                    "properties": {
                        "pixelSize": width
                    },
                    "fields": "pixelSize"
                }
            })
        return self

    def set_columns_width(self, sheet: Sheet, width: int, column_start: int,
                          columns_count: int = 1) -> 'SpreadsheetsBuilder':
        self.requests.append({
            "updateDimensionProperties": {
                "range": {
                    "sheetId": sheet.sheet_id,
                    "dimension": "COLUMNS",
                    "startIndex": column_start - 1,
                    "endIndex": column_start - 1 + columns_count,
                },
                "properties": {
                    "pixelSize": width
                },
                "fields": "pixelSize"
            }
        })
        return self

    def add_basic_filter(self,
                         sheet: Sheet,
                         range: Union[str, SpreadsheetsRange]) -> 'SpreadsheetsBuilder':
        if isinstance(range, str):
            range = SpreadsheetsRange.create_from_str(range)

        _range = {
            "sheetId": sheet.sheet_id,
        }
        if range is not None:
            if range.row_start is not None: _range["startRowIndex"] = range.row_start - 1
            if range.row_end is not None: _range["endRowIndex"] = range.row_end - 1 + 1
            if range.col_start is not None: _range["startColumnIndex"] = range.col_start - 1
            if range.col_end is not None: _range["endColumnIndex"] = range.col_end - 1 + 1

        self.requests.append({
            "setBasicFilter": {
                "filter": {
                    "range": _range,
                    "sortSpecs": [],
                    "criteria": {},
                }
            }
        })
        return self

    def add_sheet(self, placement: Union[int, SpreadsheetSheetPlacement], title: str,
                  sheet_id: int = None) -> 'SpreadsheetsBuilder':
        if isinstance(placement, SpreadsheetSheetPlacement):
            placement = placement.value

        props = {
            "title": title,
        }
        if placement is not None:
            props["index"] = placement
        if sheet_id is not None:
            props["sheetId"] = sheet_id

        self.requests.append({
            "addSheet": {
                "properties": props,
            }
        })
        return self
